## Live preview
[Here](https://stark-tor-64139.herokuapp.com/)
## How to run locally
1. Download files  or clone repo 
2. Go to project directory
3. `npm i`
4. `npm start`