import React from 'react';
import ReactDOM from 'react-dom';
import { create } from 'react-test-renderer';
import App from './App';
import { GameMenu } from './components/gameMenu';
import { PlayerCard, WinCounter } from './components/playerCard';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it(`Two player cards rendered with 0 in counter, 'FIGHT' button is disabled, 'NEW GAME' button enabled`, () => {
  const instance: any = create(<App />).root;
  const gameMenu: any = instance.findByType(GameMenu);
  const fightButton: any = gameMenu.findByProps({ className: 'fight' });
  expect(fightButton.props.disabled).toBe(true);
  const newGameButton: any = gameMenu.findByProps({ className: 'new-game' });
  expect(newGameButton.props.disabled).toBe(undefined);
  const playerCardsDrawElements: Array<any> = instance.findAllByType(PlayerCard);
  expect(playerCardsDrawElements.length).toBe(2);
  playerCardsDrawElements.forEach((playerCard: any) => {
    const counter: any = playerCard.findByType(WinCounter);
    expect(counter.props.count).toBe(0);
  });
})
