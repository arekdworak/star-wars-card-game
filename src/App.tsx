import Grid from '@material-ui/core/Grid';
import React, { Component } from 'react';
import './App.scss';
import { GameMenu } from './components/gameMenu';
import { Loader } from './components/loader';
import { PlayerCard } from './components/playerCard';
import Service from './Service';
import { generateRandomNumber } from './utils';
import { Player, Resource, ResourceType } from './vo';


interface AppState {
  selectedResource: ResourceType;
  loading: boolean;
  fightInProgress: boolean;
  people: Array<Resource>;
  starships: Array<Resource>;
  players: Array<Player>;
  canFight: boolean;
  canChangeResource: boolean;
  lastDrawedResource?: Resource;
}


export default class App extends Component {

  _isMounted = false;

  state: AppState = {
    selectedResource: ResourceType.PEOPLE,
    loading: false,
    fightInProgress: false,
    people: [],
    starships: [],
    players: [
      {
        winsCount: 0
      } as Player,
      {
        winsCount: 0
      } as Player
    ],
    canFight: false,
    canChangeResource: true
  }

  private service = new Service();

  render(): any {
    return (
      <div className="app">
        <Loader showWhen={this.state.loading || this.state.fightInProgress}></Loader>
        <Grid container spacing={3}>
          <PlayerCard
            player={this.state.players[0]}
            onDrawResource={() => { this.drawResource(0) }}>
          </PlayerCard>
          <GameMenu
            resources={[ResourceType.PEOPLE, ResourceType.STARSHIP]}
            selectedResource={this.state.selectedResource}
            canFight={this.state.canFight}
            canChangeResource={this.state.canChangeResource}
            onResourceChange={(e) => this.setState({ selectedResource: e.target.value })}
            onFight={this.onFight}
            onNewGame={this.onNewGame}>
          </GameMenu>
          <PlayerCard
            player={this.state.players[1]}
            onDrawResource={() => { this.drawResource(1) }}>
          </PlayerCard>
        </Grid>
      </div>
    );
  }

  componentDidMount() {
    this._isMounted = true;
    this.setState({ loading: true });
    Promise.all([
      this.service.getEntitiesArray(ResourceType.PEOPLE, 2),
      this.service.getEntitiesArray(ResourceType.STARSHIP, 2)
    ]).then((
      [peopleItems, starshipItems]
    ) => {
      this.setState({
        people: peopleItems.map((person: any) => {
          return {
            name: person.name,
            powerFieldName: 'mass',
            powerFieldValue: parseInt(person.mass) || 0
          } as Resource;
        }),
        starships: starshipItems.map((starship: any) => {
          return {
            name: starship.name,
            powerFieldName: 'crew',
            powerFieldValue: parseInt(starship.crew) || 0
          } as Resource;
        }),
        loading: false
      });
    }).catch(() => {
      this.setState({ loading: false });
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  private drawResource = (playerIndex: number): void => {
    let resource: Resource = this.getRandomResource();
    let newPlayer: Player = {
      ...this.state.players[playerIndex],
      currentResource: resource
    }

    let newPlayers: Array<Player> = [...this.state.players];
    newPlayers[playerIndex] = newPlayer;

    this.setState({
      players: newPlayers,
      lastDrawedResource: resource,
      canFight: newPlayers.every((player: Player) => {
        return !!player.currentResource;
      }),
      canChangeResource: !newPlayers.some((player: Player) => {
        return !!player.currentResource;
      })
    });
  }

  private onFight = (): void => {
    this.setState({ fightInProgress: true }, () => {
      setTimeout(() => {
        this.resolveFight();
      }, 1500);
    })
  }

  private resolveFight(): void {
    let newState = {
      ...this.state,
      fightInProgress: false,
      players: this.state.players.map((player: Player) => {
        return {
          ...player,
          currentResource: null
        } as Player
      }),
      canFight: false,
      canChangeResource: true,
      lastDrawedResource: null
    };
    let resource1 = this.state.players[0].currentResource;
    let resource2 = this.state.players[1].currentResource;

    if (resource1 && resource2) {
      if (resource1.powerFieldValue > resource2.powerFieldValue) {
        newState.players[0].winsCount = newState.players[0].winsCount + 1;
      } else if (resource2.powerFieldValue > resource1.powerFieldValue) {
        newState.players[1].winsCount = newState.players[1].winsCount + 1;
      }
    }
    this.setState(newState);
  }

  private onNewGame = (): void => {
    this.setState({ loading: true }, () => {
      setTimeout(() => {
        this.setState({
          loading: false,
          players: this.state.players.map((player: Player) => {
            return {
              winsCount: 0
            } as Player
          }),
          selectedResource: ResourceType.PEOPLE,
          canFight: false,
          canChangeResource: true
        });
      }, 500);
    });
  }

  private getRandomResource(): Resource {
    let resource: Resource;
    if (this.state.selectedResource === ResourceType.PEOPLE) {
      resource = this.state.people[generateRandomNumber(this.state.people.length - 1)];
    } else {
      resource = this.state.starships[generateRandomNumber(this.state.starships.length - 1)];
    }
    if (resource === this.state.lastDrawedResource) {
      return this.getRandomResource();
    } else {
      return resource;
    }
  }
}
