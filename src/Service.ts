
import axios from 'axios';
import { ResourceType } from './vo';

const SERVICE_URL: string = 'https://swapi.co/api/';

export default class Service {

    getEntitiesArray(entityName: ResourceType, pagesCount: number): Promise<Array<any>> {
        let pagePromises: Array<Promise<any>> = [];
        for (let i = 1; i <= pagesCount; i++) {
            pagePromises.push(axios.get(`${SERVICE_URL}${entityName}/?page=${i}`))
        }
        return Promise.all(pagePromises).then((pageResponses: any) => {
            return pageResponses.reduce((prevPage: any, currPage: any) => {
                return [...prevPage.data.results, ...currPage.data.results];
            });
        });
    }
}