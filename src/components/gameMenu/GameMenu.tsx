import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import React, { ChangeEvent, Component } from 'react';
import { ResourceType } from './../../vo';
import './gameMenu.scss';


export interface GameMenuProps {
    resources: Array<ResourceType>;
    selectedResource: ResourceType;
    canChangeResource: boolean;
    canFight: boolean;
    onResourceChange: (e: ChangeEvent<HTMLInputElement>) => void;
    onFight: () => void;
    onNewGame: () => void;
}

export class GameMenu extends Component<GameMenuProps> {

    render(): any {
        return (
            <Grid item xs={4}>
                <Paper
                    className="game-menu">
                    <h1>Choose resource to fight</h1>
                    <RadioGroup
                        aria-label="resource"
                        name="resource"
                        value={this.props.selectedResource}
                        onChange={this.props.onResourceChange}>
                        {this.props.resources.map((resource: string) => {
                            return <FormControlLabel
                                key={resource}
                                value={resource}
                                control={<Radio color="primary" />}
                                label={resource.toUpperCase()}
                                disabled={!this.props.canChangeResource} />
                        })}
                    </RadioGroup>
                    <Button
                        variant="contained"
                        color="primary"
                        className="fight"
                        disabled={!this.props.canFight}
                        onClick={this.props.onFight}>
                        Fight!
                    </Button>
                    <Button
                        variant="contained"
                        color="secondary"
                        className="new-game"
                        onClick={this.props.onNewGame}>
                        New Game
                    </Button>
                </Paper>
            </Grid>
        );
    }
}