import CircularProgress from '@material-ui/core/CircularProgress';
import React from 'react';
import './loader.scss';

export interface LoaderProps {
    showWhen: boolean;
}

export const Loader = (props: LoaderProps) => {
    if (props.showWhen) {
        return (
            <div className="loadingWrapper">
                <CircularProgress size={200}></CircularProgress>
            </div>
        )
    }
    return null;
}