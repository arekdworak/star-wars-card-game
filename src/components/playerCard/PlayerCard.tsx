import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import React, { Component } from 'react';
import { Player, Resource } from '../../vo';
import './playerCard.scss';
import { WinCounter } from './WinCounter';

export interface PlayerCardProps {
    player: Player;
    onDrawResource: () => void;
}

export class PlayerCard extends Component<PlayerCardProps> {

    render(): any {
        return (
            <Grid item xs={4}>
                <WinCounter count={this.props.player.winsCount}></WinCounter>
                <Card className="player">
                    <div className="player-card-container">
                        <div className="player-card">
                            {this.renderCardBack()}
                            {this.renderCardFront()}
                        </div>
                    </div>
                </Card>
            </Grid>
        )
    }

    private renderCardBack(): any {
        if (!this.props.player.currentResource) {
            return (
                <CardContent
                    className="back"
                    onClick={this.props.onDrawResource}>
                    ?
            </CardContent>
            );
        }
        return null;
    }

    private renderCardFront(): any {
        let resource: Resource | null = this.props.player.currentResource;
        if (resource) {
            return (
                <CardContent className="front">
                    <div className="info">
                        <h2>{resource.name}</h2>
                        <div className="value">
                            <span>{resource.powerFieldName}</span>
                            <span>:</span>
                            <span>{resource.powerFieldValue}</span>
                        </div>
                    </div>
                </CardContent>
            )
        }
        return null;
    }
}