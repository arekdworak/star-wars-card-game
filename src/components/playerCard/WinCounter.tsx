import React from 'react';
import './winCounter.scss';

export interface WinCounterProps {
    count: number;
}

export const WinCounter = (props: WinCounterProps) => {
    return (
        <div className="win-counter">
            <div className="count">
                {props.count}
            </div>
        </div>
    );
}