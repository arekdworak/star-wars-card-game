export enum ResourceType {
  PEOPLE = 'people',
  STARSHIP = 'starships'
}

export interface Resource {
  name: string;
  powerFieldName: string;
  powerFieldValue: number;
}

export interface Player {
  currentResource: Resource| null;
  winsCount: number;
}